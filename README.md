# Magento1_Shipping
This is Magento 1.x module for adding shipping method (the user can choose this shipping method during checkout).

After installing this module in the admin panel, the new menu item will appear in the System -> Configuration -> Shipping Methods. Its name is "Medvslav Shipping Module".
You can enter the configuration of this shipping module here.

The form has following fields:

- Enabled (for activating this shipping method in the checkout process)

- Method Name

- Title

- Price (for entering the price of this shipping method)

- Displayed Error Message

- Ship to Applicable Countries

- Ship to Specific Countries

After installing this module you have to activate this shipping method in the configuration.

You can change the price of this shipping method in the Admin panel -> System -> Configuration -> Shipping Methods -> Medvslav Shipping Module -> Prise field.
#